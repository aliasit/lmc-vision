
## Vision Mixer
***AM Start Time: 8:30am***

The vision mixer is responsible for what gets recorded, is sent to the screen and is sent to the stream.

### Pre Service

- Turn on equipment
- Turn on Stage Display
- Turn on Projectors
- Login computers as required
- Confirm all cameras are operational
- Understand order of service
- Confirm opening person
- Note down song leaders
- Confirm any special requirements for service (sermon, announcements, etc)
- Support Vision team members with equipment as required
- Understand positions on stage
- Note down any changes to service run sheet
- Test and setup Keys for Sermon and other requirements
- Test comms
- Start recording
- Start stream
***Attend Prayer Meeting at 9:30***

### Post Service

- Stop recording
- Stop stream
- Turn off cameras and confirm they are locked out
- Turn off equipment once post service slides have run for about 20 min
- Turn off projectors
- Turn off stage display


## Media Operator
***AM Start Time: 8:30***

### Pre Service

- Words available for practice
- Run through Announcements
- Understand order of service
- Import and test sermon notes as required
- Note down any changes to service run sheet
- Run Sound Stripe for stream
- Run Pre-service slides
- Check in with Vision Mixer for any special requiremetns
***Attend Prayer Meeting at 9:30***

### Post Service

- Run post-service slides
- Run Sound Stripe for stream
- Turn off sound stripe once stream has ended
- Turn off post-service slides after about 20 min 


## Camera Operator
***AM Start Time: 9:00***

- Check in with Vision Mixer upon arriving to confirm any special requirements
- Confirm camera settings accurate (F.Stop, etc.)
- Test and adjust pan and tilt settings
- Understand positions on stage
- Confirm camera angles possible
- Note down flow of service/songs during rehersal
- Think about some shots that could work with songs
- Pay attention during announcements runthrough to understand if there are any special requirements
***Attend Prayer Meeting at 9:30***
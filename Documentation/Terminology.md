# Terminology

One key for an effective team is to standardise on the terminology used across different areas. This is especially true in video production where team members are able to perform multiple roles and there is a strong reliance on short secicent verbal direction to coordinate shots.
Below is the documented terminology that is used within the LMC Vision team.

> As a general rule, when calling one of the terms below, the **Term** should be preceeded or followed by a camera number (eg. **1**) to allow the camera operators to know which camera is been given the command.  
> When it is obvious (eg. the camera just got given another command, or there is only one operator), the camera number can be left off the call.  
> All examples below will include a camera number for reference, however the **Term** column will not indicate this.

## Camera Direction

| Term | Meaning | Example |
|------|---------|---------|
| READY | Camera should prepar to be cut to program out | **_READY 2_** |
| PREVIEW | Camera should prepar to be cut to program out | **_PREVIEW 2_** |
| CUT | Camera is being cut to program | **_CUT 2_** |
| KEY UP | USK or DSK is live | **_KEY UP_** |
| KEY CLEAR | USK or DSK is now clear |  **_KEY CLEAR_** |

## Camera Movement

| Term | Meaning | Example |
|------|---------|---------|
| HOLD | Stop any camera movement and hold shot | **_HOLD 1_** |
| RESET | Return to previous position (eg. starting point) | **_RESET 1_** |
| START | Begin prescribed movement (eg. start push) | **_START 1_** |
| PAN RIGHT | Move camera lens right | **_1 PAN RIGHT_** |
| PAN LEFT | Move camera lens left | **_1 PAN LEFT_** |
| TILT UP | Move camera lens up | **_1 TILT UP_** |
| TILT DOWN | Move camera lens down | **_1 TILT DOWN_** |
| ZOOM IN | Tighten camera framing | **_ZOOM IN 1_** |
| ZOOM OUT | Loosen camera framing | **_ZOOM OUT 1_** |
| TIGHTEN UP | Zoom in slightly to tighten camera framing | **_TIGHTEN UP 1_** |
| LOOSEN UP | Zoom out slightly to loosen camera framing | **_LOOSEN UP 1_** |
| PUSH IN | Zoom in slowly on subject | **_PUSH IN 2_** |
| PULL OUT | Zoom out slowly from subject | **_PULL OUT 2_** |
| PUSH `SUBJECT` RIGHT | Slowly move subject to right side/edge of frame | **_1 PUSH SINGER RIGHT_** |
| PUSH `SUBJECT` LEFT | Slowly move subject to left side/edge of frame | **_1 PUSH SINGER LEFT_** |
| PUSH `SUBJECT` CENTER | Slowly move subject to center of frame | **_1 PUSH SINGER CENTER_** |
| LOSE `SUBJECT` RIGHT | Slowly lose subject off right side/edge of frame | **_1 LOSE SINGER RIGHT_** |
| LOSE `SUBJECT` LEFT | Slowly lose subject off left side/edge of frame | **_1 LOSE SINGER LEFT_** |
| REVEAL `SUBJECT` RIGHT | Slowly reveal subject on the right side/edge of frame | **_1 REVEAL SINGER RIGHT_** |
| REVEAL `SUBJECT` LEFT | Slowly reveal subject on the left side/edge of frame | **_1 REVEAL SINGER LEFT_** |
| STATIC SHOT | A non-moving/still shot | **_STATIC SHOT 1_** |

## COMPOSITION

| Term | Meaning | Example |
|------|---------|---------|
| FULL WIDE | Zoom out as wide as possible | **_FULL WIDE 1_** |
| STAGE WIDE | Zoom out to frame complete stage | **_STAGE WIDE 1_** |
| HEAD TO TOE | Framing of subject from head to toe | **_HEAD TO TOE 1_** |
| KNEES UP | Framing of subject from just below the knees to top of head | **_1 KNEES UP_** |
| INSEAM UP | Framing of subject from just below the inseam to top of head | **_1 INSEAM UP_** |
| WAIST UP | Framing of subject from just below belt or waistline to top of head | **_1 WAIST UP_** |
| ELBOWS UP | Framing of subject from just below the elbows to top of head | **_ELBOWS UP 1_** |
| BUST SHOT | Framing of subject from just below the shoulders to the top of head | **_BUST SHOT 1_** |
| HEAD SHOT | Framing of subject from just above the shoulders to top of head | **_HEAD SHOT 1_** |
| 1-SHOT | Framing of one subject (typically from the waist up) | **_1-SHOT 1_** |
| 2-SHOT | Framing of two subjects | **_2-SHOT 1_** |
| 3-SHOT | Framing of three subjects | **_3-SHOT 1_** |
| FRAME `SUBJECT` LEFT | Framing of subject in the left side of screen | **_1 FRAME SINGER LEFT_** |
| FRAME `SUBJECT` RIGHT | Framing of subject in the right side of screen | **_1 FRAME SINGER RIGHT_** |
| CENTER UP | Framing of subject in the center of screen | **_1 CENTER UP_** |

## MISC

| Term | Meaning | Example |
|------|---------|---------|
| CHECK FOCUS | Image is out of focus and needs to be sharpened up | **_1 CHECK FOCUS_** |
| CHECK IRIS `DARK/HOT` | Image is either too dark or too bright (adjust f-stop/iris) | **_1 CHECK IRIS HOT_** |
| HEADROOM `MORE/LESS` | Framing above the subjects head (too little or too great) | **_1 HEADROOM MORE_** |
| LEAD-ROOM | Open space in front of subject (when facing/walking left or right) | **_LEAD-ROOM 1_** |
| RACK FOCUS | Change in focal point from one subject to another or to roll out of focus | **_RACK FOCUS 1_** |
| PREVIEW | Camera should prepar to be cut to program out | **_PREVIEW 2_** |
| PROGRAM | Camera which is live | **_PROGRAM 1_** |

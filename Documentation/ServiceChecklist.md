# Sunday Service Vision Checklist

## Pre-Service

- [ ] Power on equipment under Vision desk (First power point under vision mixer)
- [ ] Power on and setup 3 cameras (put lens cap for Cam3 in Vision booth so it does not get lost)
- [ ] Power on 2 Side Projectors
- [ ] Power on Stage Display TV
- [ ] Power on ProPresenter PC

> If ProPresenter PC does not output anything to the VisionMixer, follow steps to resolve

## Post-SoundCheck

- [ ] After sound check and rehersal, ProPresenter should run Pre-Slides
- [ ] During Pre-Slides SoundStripe should play from ProPresenter PC for recording
- [ ] Set Hall2 Screen and Foyer Screen to show Program Out (For church Overflow)

## **9:50am**

- [ ] 10 minutes before the start of the service, start recording on Hyperdeck

## **9:57am**

- [ ] ProPresenter plays the 5min countdown.
- [ ] ProPresenter continues to play SoundStripe behind 5min countdown

**_CONFIRM HYPERDECK IS RECORDING - RED RECORD LIGHT SHOULD BE ON_**

## Post-Service

- [ ] ProPresenter to run Post-Slides
- [ ] During Post-Slides SoundStripe should play from ProPresenter PC

## **5min After Service**

- [ ] Stop recording on HyperDeck

## Post-Service

- [ ] ShutDown ProPresenter PC
- [ ] Turn off Projectors
- [ ] Turn off Stage Display
- [ ] Turn off Cameras, put lens cap back on Camera3
- [ ] Power down Vision room equipment (first power point under vision mixer)

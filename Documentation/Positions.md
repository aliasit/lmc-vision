# Positions

There are a number of positions in the Vision team at LMC. Below are listed the current positions and their descriptions used during services.

> Positions in _Italic_ are generally only used during special events.

## Vision Team Positions

| Position Title | Description |
|---|---|
| **Vision Team Leader** | Leader of the video team including the Vision Mixer, Media Operator(s), Camera Operator(s) and Shader; Overseas all camera direction, switching of cameras, graphics and video playback. |
| **Vision Mixer** <br /> (AKA: Technical Director) | Responsible for execution all cuts, dissolves, graphics and video playback (through the video switcher). |
| **Media Operator** <br /> (AKA: CG Operator) | Responsible for the presentation of lyrics, message notes, graphics, motion graphics and video playback during the services. |
| **Camera Operator** | Responsible for camera motion, focus adjustments, zoom and other aspects as called by the Vision Mixer during a service. |
| **_Assisatnt Director (AD)_** | _Assists the Vision Team Leader of upcoming service elements; announcing cues, transitions, song sections (intros, solos, breaks, leads), camera composition, setting up pre-determined shots, etc._ |
| **_Shader_** | _Monitors and adjusts iris/white balance of cameras equipped with camera controlled units (CCU's); equipment without CCU's must rely on the camera operator(s) for these adjustments._ |
| **_Stage Manager_** | _Responsible for working with the team on the stage, providing countdowns, cut cue's and other direction as directed by the Vision Team Leader and the Vision Mixer._ |

## Cameras Types

| Type | Description |
|---|---|
| **Stationary** | Stationary camera (with operator) located within the main auditorium (on a tripod) typically in front of the stage. |
| **Mobile** | Mobile camera (with operator) located either on-stage (handheld) or in front of the stage (handheld/tripod on dolly) typically getting close-up shots of band and instruments. AKA: Handheld/Roaming Camera |
| **Stage** | Stationary camera (with out operator) located either on-stage or in front of the stage, typically setup on a close up/medium shot of a band member and or an instrument. |

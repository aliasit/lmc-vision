# Fix output for ProPresenter 7

In the event that ProPresenter 7 starts up, but there is no output to the Vision Mixer or the Stage Display, follow the steps below to resolve.

## 1. Open the Screens menu

- In the ProPresenter application, open up the "**Screens**" menu.  
![Screens Menu](assets/01-Screens-Menu.png "Screens Menu")

## 2. Open Configure Screens

- From the drop down Screens menu, select the "**Configure Screens**" menu item.  
![Configure Screens](assets/02-Configure-Screens.png "Configure Screens")

## 3. Delete the Stage Screen

- In the "**Screen Configuration**" dialog box, right click on the "**Stage Screen**" and select "**Delete**".  
![Delete Stage Screen](assets/03-Stage-Screen-Delete.png "Delete Stage Screen")

- The "**Stage Screen**" should now be gone.  
![Stage Screen Gone](assets/04-Stage-Screen-Missing.png "Stage Screen Gone")

## 4. Re-Create the Stage Screen

- In the "**Screen Configuration**" dialog box, click the "**+**" next to "**Stage**" to add another screen.
- Select "**DeckLink Duo (2)**" from the list of displays.
- Select "**1080p50**" from the list of output options for that display.  
![Stage Screen Add](assets/05-Stage-Screen-Add.png "Stage Screen Add")  

## 5. Disable/Enable the Stage Display

- With the Stage Screen re-created, "**Disable**" the "**Stage Display**" using the toggle next to the word "**Stage**"  
![Stage Screen Disable](assets/06-Stage-Screen-Disabled.png "Stage Screen Disable")  
- Re-enable the Stage Screen  
![Stage Screen Enable](assets/07-Stage-Screen-Enabled.png "Stage Screen Enable")

> If the Toggle is already diaabled (black/gray), just enable it again (blue/gray)

## 6. Test

- Close the "**Screen Configuration**" dialog box using the "**X**"
- Test sending output to the Vision Mixer by selecting a slide, and confirm that the stage display is working
